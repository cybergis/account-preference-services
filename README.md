This project will create a web-based repository where users can self-register and store public key information and other configuration information. The owner of the instance will use a simple command line tool that will access the repository using a REST API and perform all necessary operations to create a new login on the system. 
Author: Garrett Nickel <gmnicke2@illinois.edu>

Content:
	src/account_setup/setup.py
	src/roger_registration_submit/home/home.{html,css,js}
	src/roger_registration_submit/public/bg.png
	src/roger_registration_submit/public/submit_bg.gif
	src/roger_registration_submit/public/submit_hover_bg.gif
	src/roger_registration_submit/server/routes.js
	src/roger_registration_submit/server/server-methods.js
	src/roger_registration_submit/server/smtp.js
