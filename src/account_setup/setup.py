#!/usr/bin/env python

###########################################################
# Copyright (c) 2015 CyberGIS Center for Advanced Digital #
# and Spatial Studies (CyberGIS). All Rights Reserved     #
###########################################################
# setup.py: Command-line utility for automatic VM account setup
# Author: Garrett Nickel <gmnicke2@illinois.edu>
# Date: 08/20/2015

#todo
#new repo setuptools
#test on virtual machine

"""
Provides two utilities to:
    1) List new OpenStack users that have used the online form to register preferences
    2) Take an OpenStack user's preferences from the database and register them in the filesystem with their preferences and public SSH key

**Important:** for adduser, this utility must be run as root

List (list):
    List all users who have registered through the form and the important info:
        
        # default execution behavior of this program is to List
        ./setup.py
        # or
        ./setup.py list

        # print info about a specific user with -u or --user=
        # can also specify REST endpoint with --endpoint=<endpoint>
        # by default, is it where the meteor app is currently deployed
        ./setup.py --user=<user> --endpoint=<endpoint>

Add User (adduser):
    Add the user with or without sudo-er privileges to the current filesystem, as well as extract pertinent preference files and public SSH key
    
        # add a user without sudo privileges
        sudo ./setup.py --user=<user> adduser
        # to add a list of user : 
        sudo python setup.py adduser --user user1 user2

        # add a user with sudo privileges
        sudo ./setup.py --user=<user> --sudo adduser
        # a prompt confirming if the user should really be a sudoer will appear

Search User (search):
    Search user based on keyword provided in full name as well as user name
    Example : sudo python setup.py --user=dun search

List all the group name : 
    python setup.py listgroup

Add group :
    sudo python setup.py addgroup --group=group_name
"""
import os
import sys
import json
import time
import getpass
import requests
import argparse
import subprocess
from requests import exceptions as rex

def parse_args() :
    """Defines command-line positional and optional arguments a nd checks
        for valid action input if present.

    Args: none

    Returns: A (tuple) containing the following:
        args (namespace) : used to provide certain variables
        action (string) : for main to use as a switch for calls to perform
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("action", nargs='?', type=str, default='list',
        help='list (list users) / adduser (add user & preferences) / removeuser (remove user & preferences) / addgroup (add group & preferences)')
    parser.add_argument("--sudo",
        action='store_true',
        help="User is to have sudo privileges")
    parser.add_argument("-e", "--endpoint",
        default="http://141.142.168.56/",
        help="Endpoint for the RESTful query")
    parser.add_argument("-s", "--server", 
                        help = "provide the link to the preferrable files")
    parser.add_argument("-l","--login", 
                        help = "provide the login name")
    parser.add_argument("-g", "--group", 
                        help = "specify the group to be added")
    parser.add_argument("-u", "--user", nargs='*',
                        help = "provide information of a user")
    parser.add_argument("--adduser", action="store_true",
                        help="add user to the system")
    parser.add_argument("--addgroup", action="store_true",
                        help="add user to the system")
    parser.add_argument("--removeuser", action="store_true",
                        help="remove user from the system")
    parser.add_argument("--notify", choices = ["email", "sms"], 
                        help="notify the user using specified method")
    parser.add_argument("--search", action="store_true",
                        help="search for user")
    parser.add_argument("--listgroup", action="store_true",
                        help="list all the group name")

    args = parser.parse_args()

    if not args.endpoint :
        print('Endpoint for REST API not specified')
        sys.exit(1)

    if args.action.lower() == 'search' :
        return (args, args.action.lower())
    if args.action.lower() == 'adduser' and args.user == "" :
        args.user = raw_input("Please specify which user to add: ")

    if args.sudo and args.action.lower() == 'adduser' and args.sudo == True :
        sudoer = raw_input("User is to be sudo-er, confirm? (Y/n): ")
        if(sudoer != 'Y') :
            args.sudo = False
            print('User "'+args.user+'" will not be sudo-er.');
        else :
            print('User "'+args.user+'" will be sudo-er.');
    else :
        args.sudo = False
        if(args.action.lower() == 'adduser') :
            print('User "'+args.user[0]+'" will not be sudo-er.');

    return (args, args.action.lower())

def rest(endpoint, resource, method,  **kwargs) :
    """Calls the REST endpoint passing any necessary keyword arguments
        'rest' is a basic wrapper around the HTTP request to the REST endpoint

    Args:
        endpoint (str, URL): the REST endpoint
        resource (str): the resource to query
        method (str): the HTTP method that will be called
        kwargs (optional): keywords to optionally include

    Raises:
        Raises exceptions resulting from errors invoking the requests module
        functions
    """

    url = endpoint.rstrip('/') + '/' + resource
    if(method.upper()=='POST') :
        r = requests.request(method.upper(), url, timeout=50, data=kwargs)
    else :
        r = requests.request(method.upper(), url, timeout=50, params=kwargs)

    r.raise_for_status()

    response = r.json()

    return response

def execute_shell(commands) :
    """Creates and waits for subprocesses that execute shell commands involved
        with various filesystem creation steps. Prints each command as it is
        executed, as well as all non-empty stdout piped into the subprocess

    Args:
        commands (list): list of bash shell commands to iterate through

    Returns:
        void
    """

    for i in range(0,len(commands)) :
        print('Executing "'+commands[i]+'"')
        process = subprocess.Popen(commands[i], shell=True, stdout=subprocess.PIPE)
        output = process.communicate()[0]
        if(output != "") :
            print(output)
        process.wait()
        print("..................")
        time.sleep(1)


def create_account(username, full_name) :
    """Executes the first step -- simple account creation

    Args:
        username (string): username of the new user
        full_name (string): full name of the new user

    Returns:
        void
    """

    commands = ['useradd -c "'+full_name+'" -s /bin/bash -m '+username]
    execute_shell(commands)
    print('Account "'+username+'" created, home directory at ~'+username)

def authorize_key(username, public_key) :
    """Handles copying the user's SSH public key into authorized_keys

    Args:
        username (string): username of the new user
        public_key (string): public SSH key of the new user

    Returns:
        void
    """

    ssh_dir = '/home/'+username+'/.ssh'

    commands = [
        'mkdir '+ssh_dir,
        'chmod 700 '+ssh_dir,
        'echo "'+public_key+'" > '+ssh_dir+'/authorized_keys',
        'chmod 600 '+ssh_dir+'/authorized_keys',
        'chown -R '+username+':'+username+' '+ssh_dir
    ]
    execute_shell(commands)

def handle_preferences(username, repo) :
    """Handles pulling from and copying information/files from the user's
        preference repo. This includes creating a new .vimrc from the user's
        as well as appending to .bashrc

    Args:
        username (string): username of the new user
        repo (string): code repository of the user's preference files

    Returns:
        void
    """

    user_dir = '/home/'+username
        
    commands = [
        'git clone '+repo+' '+user_dir+'/preferences',
        'sleep 3',
        'mv '+user_dir+'/preferences/* '+user_dir+'/',
        'cat '+user_dir+'/preferences/.vimrc >'+user_dir+'/.vimrc',
        'cat '+user_dir+'/preferences/.bashrc >>'+user_dir+'/.bashrc',
        'rm -rf '+user_dir+'/preferences'
        ]
    execute_shell(commands)

def make_sudoer(username) :
    """Executed if --sudo=True on execution, handles making the user a sudoer
        and generates a random password in ~user/home/.password

    Args:
        username (string): username of the new user

    Returns:
        void
    """

    commands = [
        'adduser '+username+' sudo',
        'openssl rand -base64 6 | sudo -u '+username+' tee -a /home/'+username+'/.password',
        'chmod 600 /home/'+username+'/.password',
        'passwd '+username
    ]
    execute_shell(commands)

def create_user(endpoint, username, is_sudoer) :
    """Called when the action is 'adduser', calls all functions to create
        a new user after RESTfully GETting the user's information

    Args:
        endpoint (string, URL): the REST endpoint
        username (string): username of the new user
        is_sudoer (bool): True is user is to be sudoer, else False

    Returns:
        void
    """
    if os.geteuid() != 0 :
         exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
    for i in range (0, len(username)) : 
        resource = 'users/'+username[i]
        user_list = rest(endpoint, resource, 'GET')
        print user_list 
        if(len(user_list) > 0) :
            user_info = user_list[0]
            full_name = user_info['full_name']
            public_key = user_info['public_key']
            repo = user_info['pref_repo']
            create_account(username[i], full_name)
            if(is_sudoer) :
                make_sudoer(username[i])
            authorize_key(username[i], public_key)
            handle_preferences(username[i], repo)
        else :
            print('User "'+username[i]+'" has not submitted a preference form.')

def create_group(endpoint, groupname, is_sudoer) :
    if os.geteuid() != 0 :
         exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
    resource = 'users?groups=' + groupname
    group_result = rest(endpoint, resource, 'GET')
    print group_result
    if(len(group_result) > 0) :
        for i in range(0, len(group_result)) :
            create_user(endpoint, [group_result[i]['username']], is_sudoer)
    else :
        print 'No group with specified group name found'
    sys.exit(1)

def print_group_name(endpoint):
    resource='groups'
    response = rest(endpoint, resource, 'GET')
    if (len(response) > 0) :
        for i in range(0,len(response)) :
            print('Team name : ' + response[i]['name'] + 
                 ' description : ' + response[i]['description'])
    else : 
        print('No group found')
    sys.exit(1)



def search_user(endpoint, keyword):
    if (keyword != "") :
        user_name = 'users?userName='
        user_response = rest(endpoint, user_name + keyword, 'GET')
        full_name = 'users?fullName='
        full_response = rest(endpoint, full_name + keyword, 'GET')
        response = dedup_concat((user_response,full_response))
        if (len(response) > 0) :
            for i in range (0, len(response)) :
                print('Full Name: '+response[i]['full_name'] 
                    + ' User Name: '+response[i]['username'] 
                    + ' Email: '+response[i]['email'])
               
        else :    
            print('User "'+keyword+'" Can not be found.')
    else :
        print('Please specify keyword') 
    sys.exit(1)

def dedup_concat(responses):
    key_set=set()
    result=json.loads('[]')
    for response in responses:
        for i in range(0, len(response)):
            key = ''.join((response[i]['user_id'],response[i]['_id']))
            if key not in key_set:
                key_set.add(key)
                result.append(response[i])
    return result

def print_user_info(endpoint, username) :
    """Prints all users who have registered with the form and pertinent info

    Args:
        endpoint (string): the REST endpoint
        username (string): username of the new user

    Returns:
        void
    """
    resource = 'users'
    if username is not None:
        for i in range(0, len(username)) :
            if (username[i] != "") :
                response = rest(endpoint, resource+"/"+username[i], 'GET')
                if (len(response) > 0) :
                    print('++++++++++++++++++++++++++++++++++++++++++++++')
                    print('Info for user "'+response[0]['username']+'"')
                    print('++++++++++++++++++++++++++++++++++++++++++++++')
                    print('Full Name: \n\t'+response[0]['full_name'])
                    print('-----------------------------------------')
                    print('Public Key: \n\t'+response[0]['public_key'])
                    print('-----------------------------------------')
                    print('Preference Repo: \n\t'+response[0]['pref_repo'])
                else :
                    print('User "'+username[i]+'" has not submitted a form.')
                    sys.exit(1)
    else :
        response = rest(endpoint, resource, 'GET')
        for j in range(0,len(response)) :
            print('++++++++++++++++++++++++++++++++++++++++++++++')
            print('Info for user "'+response[j]['username']+'"')
            print('++++++++++++++++++++++++++++++++++++++++++++++')
            print('Full Name: \n\t'+response[j]['full_name'])
            print('-----------------------------------------')
            print('Public Key: \n\t'+response[j]['public_key'])
            print('-----------------------------------------')
            print('Preference Repo: \n\t'+response[j]['pref_repo'])

def main() :
    (args, action) = parse_args()
    if (action.lower() == 'list') :
        print_user_info(args.endpoint, args.user)
    elif (action.lower() == 'search') :
        search_user(args.endpoint, args.user[0])
    elif (action.lower() == 'listgroup') :
        print_group_name(args.endpoint)
    elif (action.lower() == 'adduser') :
        create_user(args.endpoint, args.user, args.sudo)
    elif (action.lower() == 'addgroup') :
        create_group(args.endpoint, args.group, args.sudo)
if __name__ == "__main__" :
    main();
