Command-line utility to automatically set up an account on an OpenStack VM.
Draws from the MongoDB database on the Meteor App that the user should submit
preferences on, including a GitHub Repo containing .vimrc, .bashrc, etc...
Details on running the utility is in the header of setup.py

Content:
	setup.py
