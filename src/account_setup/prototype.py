import argparse
import os

def main() :
	parser = argparse.ArgumentParser()
	
	parser.add_argument("-s", "--server", nargs=1, 
						help = "provide the link to the preferrable files")
	parser.add_argument("-l","--login", nargs=1, 
						help = "provide the login name")
	parser.add_argument("-g", "--group", nargs=1, 
						help = "specify the group to be added")
	parser.add_argument("-u", "--user", nargs=1, 
						help = "provide information of a user")
	parser.add_argument("--adduser", action="store_true",
	                    help="add user to the system")
	parser.add_argument("--adduserlist", nargs='*',
                    	help="add user list to the system")
	parser.add_argument("--removeuser", action="store_true",
                    	help="remove user from the system")
	parser.add_argument("--notify", choices = ["email", "sms"], 
						help="notify the user using specified method")
	if os.geteuid() != 0:
		 exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
	args = parser.parse_args()
	

##print ''.join(args.server)
##if args.login != None:
##	print 
##print ''.join(args.user)
##if args.adduser:
##	print "lol"

if __name__ == "__main__" :
    main();