Meteor client and server HTML/CSS/Javascript files and images, along with
meteor boilerplate, defining the OpenStack Preference Submission form

Content:
	home/home.{html,css,js}
	public/bg.png
	public/submit_bg.gif
	public/submit_hover_bg.gif
	server/routes.js
	server/server-methods.js
	server/smtp.js
